This repository is for the purpose of learning and organizing camp2code.

There are different branches for the parts of the workshop series.

- coachmarkets: Contains the content of the weekly sessions based on LinkedIn Learning.
- bootcamp: Contains the code for the bootcamp of the project.
- project_phase_1: Contains the code for the first phase of the project.
- project_phase_2: Contains the code for the second phase of the project.
- car_test: Allows testing the car's components.
- training_content: Contains the content of the weekly seesions based on Pluralsight
